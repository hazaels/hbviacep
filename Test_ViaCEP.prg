/*******************************************************************************
 *  Test the use of classes to check the Brazilian postal code (CEP) 
 *
 *  It uses the free service: ViaCEP (http://viacep.com.br)
 *  Visit the site to learn more
 * 
 *  Special thanks to Leandro and Franco for offering the free service!
 * 
 *  Thanks to Eric for the idea!
 *
 *
 *  Copyright (c) 2015 - Mario Wan Stadnik (Hazael)
 *  wanstadnik(at)gmail.com
 *
 *  Free to the public domain
 *
 *  
 *  To build:  HBMK2 Test_ViaCEP hbtip.hbc -gtwvt
 *
 */
 
announce HB_GTSYS
request  HB_GT_WVT_DEFAULT
request  HB_CODEPAGE_FRISO
 
PROCEDURE Main( cCEP )

   LOCAL oCep

   hb_cdpSelect( 'FRISO' )
   SETCOLOR( 'N/W*,W+/B*' )
	SETMODE( 15,80)
	SET CONFIRM ON

   CLS
	
	DO WHILE .T.
	   cCEP := SPACE( 8 )
	   
      @ 01,02 SAY "CEP...:" GET cCEP PICT '@R 99999-999' VALID( LEN( ALLTRIM( cCEP ) ) == 8 )
	   @ MAXROW()-1,02 SAY "Digite o CEP ou tecle <ESC> para encerrar" COLOR 'R+/W*'
	   READ
	   IF LASTKEY() == 27
	      EXIT
	   ENDIF
		   
	   CLS
	   
      oCep := cepWeb( cCEP )
      IF oCep == NIL
         ALERT( "Erro! Tente novamente" )
         LOOP
      ENDIF
      ?   
      ?
      ? '  CEP...:', oCep:cCEP
      ? '  IBGE..:', oCep:cIBGE
      ? '  LOGRAD:', oCep:cLogradouro
      ? '  COMPLE:', oCep:cComplemento
      ? '  BAIRRO:', oCep:cBairro
      ? '  CIDADE:', oCep:cLocalidade
      ? '  UF....:', oCep:cUF
      
   ENDDO
	  
RETURN


/*******************************************************************************
 *  This is the function and the class that it uses to work
 *  The engine of this is the contrib 'hbtip'
 *
 *  It uses the free service: ViaCEP (http://viacep.com.br)
 *  Visit the site to learn more
 * 
 *  Special thanks to Leandro and Franco for offering the free service!
 * 
 *  Thanks to Eric for the idea!
 *
 *
 *  Copyright (c) 2015 - Mario Wan Stadnik (Hazael)
 *  wanstadnik(at)gmail.com
 *
 *  Free to the public domain
 *
 *  
 *  To build:  HBMK2 Test_ViaCEP hbtip.hbc -gtwvt
 *
 */
FUNCTION cepWeb( cCEP)
   LOCAL oCEP := ViaCEP():New( cCEP )
RETURN oCEP


/* 
 *  ViaCEP Class
 */

#include 'hbclass.ch'

CREATE CLASS ViaCEP
   VAR oCep
   VAR cCep         INIT '' 
   VAR cIBGE        INIT '' 
   VAR cLogradouro  INIT '' 
   VAR cComplemento INIT '' 
   VAR cBairro      INIT '' 
   VAR cLocalidade  INIT '' 
   VAR cUF          INIT '' 
   METHOD New( cCEP )
ENDCLASS

   
METHOD New( cCEP )

   oHttp := TIPClientHTTP():new( "http://viacep.com.br/ws/" + cCEP + "/piped/" )

   IF ! oHttp:open()
      RETURN NIL
   ENDIF

   cHtml := oHttp:readAll()
   oHttp:close()
	cHtml := HB_UTF8TOSTR( cHtml ) 
	aHtml := hb_aTokens( cHtml, '|' )
	
	IF LEN( aHtml ) < 7
      RETURN NIL
   ENDIF
   
   cCEP         := SUBSTR( aHtml[1], AT( ':', aHtml[1] ) + 1 )
   cLogradouro  := SUBSTR( aHtml[2], AT( ':', aHtml[2] ) + 1 )
   cComplemento := SUBSTR( aHtml[3], AT( ':', aHtml[3] ) + 1 )
   cBairro      := SUBSTR( aHtml[4], AT( ':', aHtml[4] ) + 1 )
   cLocalidade  := SUBSTR( aHtml[5], AT( ':', aHtml[5] ) + 1 )
   cUF          := SUBSTR( aHtml[6], AT( ':', aHtml[6] ) + 1 )
   cIBGE        := SUBSTR( aHtml[8], AT( ':', aHtml[8] ) + 1 )

   ::cCEP         := cCEP
   ::cLogradouro  := cLogradouro
   ::cComplemento := cComplemento
   ::cBairro      := cBairro
   ::cLocalidade  := cLocalidade
   ::cUF          := cUF
   ::cIBGE        := cIBGE
   
RETURN Self
  