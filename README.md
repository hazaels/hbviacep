# README #

Welcome to this repository.
Here you will find information about the project and all the steps necessary to build and use it.
It is all made in [Harbour](https://harbour.github.io/)

To build just execute: `hbmk2 Test_ViaCEP hbtip.hbc -gtwvt`

### What is this repository for? ###

* [ViaCEP](https://viacep.com.br/) é um serviço gratuito e de alto desempenho para consultar Códigos de Endereçamento Postal (CEP) do Brasil.
* **Release 0.1**
* O programa é um exemplo de como integrar o serviço ViaCEP com a linguagem Harbour.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* [Download Harbour](https://harbour.github.io/) and [Install it](http://www.datacenter.fr.eu.org/xbase/guide/html/index.html)
* Download the repository source code in a folder
* Open the command prompt on the repository folder 
* Build it: `hbmk2 Test_ViaCEP hbtip.hbc -gtwvt`
* It will give you the .EXE
* Execute it!

### Who do I talk to? ###

* [Hazael](emailto:wanstadnik@gmail.com)